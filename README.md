# Codebase release 3.4 for NetKet

by Filippo Vicentini, Damian Hofmann, Attila Szabó, Dian Wu, Christopher   Roth, Clemens Giuliani, Gabriel Pescia, Jannes Nys, Vladimir Vargas-Calderón,   Nikita Astrakhantsev, Giuseppe Carleo

SciPost Phys. Codebases 7-r3.4 (2022) - published 2022-08-24

[DOI:10.21468/SciPostPhysCodeb.7-r3.4](https://doi.org/10.21468/SciPostPhysCodeb.7-r3.4)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.7-r3.4) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Filippo Vicentini, Damian Hofmann, Attila Szabó, Dian Wu, Christopher   Roth, Clemens Giuliani, Gabriel Pescia, Jannes Nys, Vladimir Vargas-Calderón,   Nikita Astrakhantsev, Giuseppe Carleo

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.7-r3.4](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.7-r3.4)
* Live (external) repository at [https://github.com/netket/netket/releases/tag/v3.4.3](https://github.com/netket/netket/releases/tag/v3.4.3)
